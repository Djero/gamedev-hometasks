#include "canvas.hpp"
#include <fstream>
#include <iostream>

canvas::canvas(size_t width, size_t height)
    : width(width)
    , height(height)
{
    pixels.resize(width * height);
}

pixel canvas::get_pixel(size_t x, size_t y)
{
    size_t linear_index = width * y * x;
    pixel  pixel        = pixels.at(linear_index);

    return pixel;
}

void canvas::set_pixel(pixel pixel_to_set)
{
    size_t linear_index =
        width * pixel_to_set.position.y + pixel_to_set.position.x;
    pixel& target_pixel = pixels.at(linear_index);
    target_pixel        = pixel_to_set;
}

void canvas::save_image()
{
    std::string   file_name = "1-canvas.ppm"; // TODO: move to better place
    std::ofstream out_file;
    out_file.open(file_name, std::ios_base::binary);

    if (!out_file)
    {
        std::cout << "error: failed to open file for writing " << file_name
                  << std::endl;
    }

    out_file << "P6\n" << width << ' ' << height << '\n' << 255 << '\n';

    std::vector<color> colors;
    for (auto p : pixels)
    {
        colors.push_back(p.color);
    }

    out_file.write(reinterpret_cast<const char*>(colors.data()),
                   sizeof(color) * colors.size());
    out_file.close();
}

void canvas::load_image()
{
    std::string        file_name = "1-canvas.ppm"; // TODO: move to better place
    std::ifstream      in_file;
    std::string        file_format;
    size_t             width;
    size_t             hight;
    std::string        color_format;
    std::vector<color> colors;

    in_file.open(file_name, std::ios_base::binary);

    if (!in_file)
    {
        std::cout << "error: failed to open file for reading" << file_name
                  << std::endl;
    }

    in_file >> file_format >> width >> hight >> color_format >> std::ws;
    in_file.read(reinterpret_cast<char*>(colors.data()),
                 sizeof(color) * pixels.size());
    in_file.close();

    for (int i = 0; i < pixels.size(); i++)
    {
        pixels[i].color = colors[i];
    }
}