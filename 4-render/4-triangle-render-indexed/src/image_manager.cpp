#include <fstream>
#include <iostream>

#include "../include/image_manager.hpp"

void image_manager::save_image(canvas& canvas)
{
    std::string file_name = "4-triangles-indexed.ppm";
    std::ofstream out_file;
    out_file.open(file_name, std::ios_base::binary);

    if (!out_file)
    {
        std::cout << "error: failed to open file for writing " << file_name
                  << std::endl;
    }

    out_file << "P6\n"
             << canvas.width << ' ' << canvas.height << '\n'
             << 255 << '\n';

    std::vector<color> colors;
    for (auto pixel_color : canvas)
    {
        colors.push_back(pixel_color);
    }

    out_file.write(reinterpret_cast<const char*>(colors.data()),
                   sizeof(color) * colors.size());
    out_file.close();
}

void image_manager::load_image(canvas& canvas)
{
    std::string file_name = "4-triangles-indexed.ppm";
    std::ifstream      in_file;
    std::string        file_format;
    size_t             width;
    size_t             hight;
    std::string        color_format;
    std::vector<color> colors;

    in_file.open(file_name, std::ios_base::binary);

    if (!in_file)
    {
        std::cout << "error: failed to open file for reading" << file_name
                  << std::endl;
    }

    in_file >> file_format >> width >> hight >> color_format >> std::ws;
    in_file.read(reinterpret_cast<char*>(colors.data()),
                 sizeof(color) * canvas.size());
    in_file.close();

    for (int i = 0; i < canvas.size(); i++)
    {
        canvas[i] = colors[i];
    }
}