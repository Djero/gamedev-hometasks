#pragma once

#include "canvas.hpp"

class render
{
public:
    render(canvas& canvas, color color_to_render);

    void render_line(position start, position end);
    void render_triangle(position v1, position v2, position v3);
    void render_triangles(std::vector<position> vertexes,  std::vector<uint8_t> indexes);
    void clear_white();
    void clear_black();

private:
    canvas& inner_canvas;
    color   color_to_render;
};