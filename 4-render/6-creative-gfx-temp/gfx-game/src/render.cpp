#include <algorithm>
#include <cmath>

#include "../include/image_file_manager.hpp" // TODO: remove
#include "../include/render.hpp"

render::render(canvas&        inner_canvas,
               gfx_processor& inner_gfx_processor,
               color          color_to_render)
    : canvas_(inner_canvas)
    , color_(color_to_render)
    , gfx_processor_(inner_gfx_processor)
{
}

void render::clear_white()
{
    color white{ color{ 255, 255, 255 } };
    std::fill(canvas_.begin(), canvas_.end(), white);
}

void render::clear_black()
{
    color black{ color{ 0, 0, 0 } };
    std::fill(canvas_.begin(), canvas_.end(), black);
}

void render::render_line(position start, position end)
{
    image_file_manager im;
    auto pixels_to_render = canvas_.get_line_pixel_positions(start, end);

    std::for_each(
        pixels_to_render.begin(), pixels_to_render.end(), [&](auto& position) {
            canvas_.set_pixel(position, color_);
            // im.save_image(canvas); // Uncomment to generate an image per
            // rendered pixel
        });
}

void render::render_triangle(position v1, position v2, position v3)
{
    render_line(v1, v2);
    render_line(v2, v3);
    render_line(v3, v1);
}

void render::render_triangles(std::vector<position> vertexes,
                              std::vector<uint8_t>  indexes)
{
    for (size_t i = 0; i < indexes.size() / 3; i++)
    {
        uint8_t i1 = indexes.at(i * 3);
        uint8_t i2 = indexes.at(i * 3 + 1);
        uint8_t i3 = indexes.at(i * 3 + 2);

        position v1 = vertexes.at(i1);
        position v2 = vertexes.at(i2);
        position v3 = vertexes.at(i3);

        render_triangle(v1, v2, v3);
    }
}

void render::render_gradient_triangles(std::vector<vertex>   vertexes,
                                       std::vector<uint16_t> indexes)
{
    for (size_t i = 0; i < indexes.size() / 3; i++)
    {
        uint8_t i1 = indexes.at(i * 3);
        uint8_t i2 = indexes.at(i * 3 + 1);
        uint8_t i3 = indexes.at(i * 3 + 2);

        vertex v1 = vertexes.at(i1);
        vertex v2 = vertexes.at(i2);
        vertex v3 = vertexes.at(i3);

        vertex v1_processed = gfx_processor_.vertex_shader(v1);
        vertex v2_processed = gfx_processor_.vertex_shader(v2);
        vertex v3_processed = gfx_processor_.vertex_shader(v3);

        std::vector<vertex> rasterized_triangle =
            rasterize_triangle(v1_processed, v2_processed, v3_processed);

        for (auto processed_vertex : rasterized_triangle)
        {
            color processed_color = gfx_processor_.fragment_shader(processed_vertex);
            position processed_position = {
                static_cast<int32_t>(std::round(processed_vertex.f0)),
                static_cast<int32_t>(std::round(processed_vertex.f1))
            };
            canvas_.set_pixel(processed_position, processed_color);
        }
    }
}

std::vector<vertex> render::rasterize_line(vertex start, vertex end)
{
    using namespace std;
    std::vector<vertex> result; // TODO: check on memory leak
    size_t              line_length = static_cast<size_t>(
        round(sqrt(pow(start.f0 - end.f0, 2) + pow(start.f1 - end.f1, 2))));

    for (size_t i = 0; i <= line_length; i++)
    {
        double t = static_cast<double>(i) / line_length;
        vertex interpolated_pixel = interpolate(start, end, t);
        result.push_back(interpolated_pixel);
    }

    return result;
}

std::vector<vertex> render::rasterize_triangle(vertex v1, vertex v2, vertex v3)
{
    std::vector<vertex> result;

    std::vector<vertex> base = rasterize_line(v1, v3); //TODO: Random

    for(auto v : base)
    {
        std::vector<vertex> rasterized_line = rasterize_line(v2, v);
        result.insert(end(result), begin(rasterized_line), end(rasterized_line));
    }

    return result;
}

double render::interpolate(double a, double b, double t)
{
    return a + (b - a) * t;
}

vertex render::interpolate(vertex v1, vertex v2, double t)
{
    double f0 = interpolate(v1.f0, v2.f0, t);
    double f1 = interpolate(v1.f1, v2.f1, t);
    double f2 = interpolate(v1.f2, v2.f2, t);
    double f3 = interpolate(v1.f3, v2.f3, t);
    double f4 = interpolate(v1.f4, v2.f4, t);
    double f5 = interpolate(v1.f5, v2.f5, t);
    double f6 = interpolate(v1.f6, v2.f6, t);

    return vertex{ f0, f1, f2, f3, f4, f5, f6 };
}
