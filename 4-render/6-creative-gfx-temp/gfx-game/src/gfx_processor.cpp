#include "../include/gfx_processor.hpp"
#include <cmath>

void gfx_processor::set_uniforms(uniforms u)
{
    uniforms_ = u;
}

void gfx_processor::set_texture(std::vector<color> t)
{
    texture_ = t;
}

vertex gfx_processor::vertex_shader(vertex v_in)
{
    vertex out = v_in;

    //    // rotate
    //    double alpha = 3.14159 / 6; // 30 degree
    //    double x     = out.f0;
    //    double y     = out.f1;
    //    out.f0       = x * std::cos(alpha) - y * std::sin(alpha);
    //    out.f1       = x * std::sin(alpha) + y * std::cos(alpha);
    //
    //    // scale into 3 times
    //    out.f0 *= 0.3;
    //    out.f1 *= 0.3;
    //
    //    // move
    //    out.f0 += (inner_uniforms.f0 / 2);
    //    out.f1 += (inner_uniforms.f1 / 2);

    return out;
}

color gfx_processor::fragment_shader(vertex v_in)
{
    color out;

    out.r = static_cast<uint8_t>(v_in.f2 * 255);
    out.g = static_cast<uint8_t>(v_in.f3 * 255);
    out.b = static_cast<uint8_t>(v_in.f4 * 255);

//    if(!texture.empty())
//    {
//        uint32_t f5 = static_cast<uint32_t>(std::round(v_in.f5));
//        uint32_t f6 = static_cast<uint32_t>(std::round(v_in.f6));
//
//        uint16_t linear_index_in_texture = f5 * inner_uniforms.f0 + f6;
//
//        color texture_color = texture.at(linear_index_in_texture);
//
//        out.r += texture_color.r;
//        out.g = texture_color.g;
//        out.b += texture_color.b;
//    }

    return out;
}