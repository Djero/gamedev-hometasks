#include <iostream>
#include <map>

#include "../include/iengine.hpp"
#include "SDL.h"

class gfx_engine : public iengine
{
public:
    void shut_down() override
    {
        SDL_DestroyWindow(_window);
        SDL_Quit();
    }

    bool process_input() override
    {
//        // map SDL keycodes to engine's actions
//        const static std::map<SDL_Keycode, ActionName>
//            sdl_to_engine_mappings = {
//            { SDLK_w, ActionName::UP },       { SDLK_a, ActionName::LEFT },
//            { SDLK_s, ActionName::BACK },     { SDLK_d, ActionName::RIGHT },
//            { SDLK_i, ActionName::BTN1 },     { SDLK_j, ActionName::BTN2 },
//            { SDLK_k, ActionName::BTN3 },     { SDLK_l, ActionName::BTN4 },
//            { SDLK_ESCAPE, ActionName::QUIT }
//        };
//
//        // get SDL event
//        static SDL_Event event; // TODO: test static
//        SDL_PollEvent(&event);
//
//        // quit on dedicated SDL event
//        if (event.type == SDL_QUIT)
//        {
//            return false;
//        }
//
//        // we are not interested in other types of events for now
//        if(event.type != SDL_KEYDOWN && event.type != SDL_KEYUP)
//        {
//            return true;
//        }
//
//        // look for engine's action name by SDL keycode
//        auto mapping = sdl_to_engine_mappings.find(event.key.keysym.sym);
//
//        // return if not found, but program continues running
//        if (mapping == std::end(sdl_to_engine_mappings))
//        {
//            action.action_name = ActionName::UNKNOWN;
//            return true;
//        }
//
//        // quit if dedicated key is pressed
//        if (action.action_name == ActionName::QUIT)
//        {
//            return false;
//        }
//
//        // store engine's action name for future output
//        action.action_name = mapping->second;
//
//        // determine key state
//        switch (event.type)
//        {
//            case SDL_KEYDOWN:
//                action.pressed = true;
//                break;
//            case SDL_KEYUP:
//                action.pressed = false;
//                break;
//        }

        return true;
    };

private:
    SDL_Window* _window = nullptr;
};

iengine* create_engine()
{
    iengine* engine = new gfx_engine;
    return engine;
};

void destroy_engine(iengine* engine)
{
    delete engine;
};

iengine::~iengine(){};
