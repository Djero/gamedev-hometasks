#include <cstdlib>
#include <string>

#include "include/hot_loader.hpp"
#include "include/iengine.hpp"

int main(int, char**)
{
    std::string game_lib_name = "game";
    hot_loader  hot_loader(game_lib_name);

    iengine* engine = create_engine();
    igame* game = hot_loader.load_game();

    while (engine->process_input())
    {
        if (hot_loader.is_game_updated())
        {
            game = hot_loader.load_game();
        }

        game->on_action();
        game->simulate();
        game->on_output();
    }

    hot_loader.unload_game();
    destroy_engine(engine);

    return EXIT_SUCCESS;
}