#include "include/canvas.hpp"
#include "include/image_file_manager.hpp"
#include "include/render.hpp"
#include <cmath>

int main(int, char**)
{
    size_t width  = 30;
    size_t height = 30;
    color  color{ 255, 0, 0 };

    uniforms uniforms{ width, height };

    gfx_processor      gfx_processor;
    canvas             canvas(width, height);
    render             render(canvas, gfx_processor, color);
    image_file_manager image_saver;

//    vertex v1{ 0, 599, 1, 0, 0 , 0, 0};
//    vertex v2{ 0, 0, 0, 1, 0, 200, 0};
//    vertex v3{ 799, 599, 0, 0, 300, 200, 0 };

//    vertex v1{ 0, 599, 1, 0, 0 , 0, 0};
//    vertex v2{ 0, 0, 0, 1, 0, 0, 0};
//    vertex v3{ 599, 0, 0, 0, 1, 0, 0 };

    vertex v1{ 0, 0, 1, 0, 0 , 0, 0};
    vertex v2{ 599, 599, 0, 1, 0, 0, 0};

    render.clear_white();
    auto x = render.rasterize_line(v1, v2);
    for(auto y : x)
    {
        position processed_position = {
            static_cast<int32_t>(std::round(y.f0)),
            static_cast<int32_t>(std::round(y.f1))
        };
        canvas.set_pixel(processed_position, color);
    }
    image_saver.save_image(canvas);


//    gfx_processor.set_uniforms(uniforms);
//    render.render_gradient_triangles({ v1, v2, v3 }, { 0, 1, 2 });
//    image_saver.save_image(canvas);
//
//    gfx_processor.set_texture(canvas);
//    render.clear_black();
//    render.render_gradient_triangles({ v1, v2, v3 }, { 0, 1, 2 });
//    image_saver.save_image(canvas);

    return EXIT_SUCCESS;
}