#pragma once

#include "canvas.hpp"

class image_file_manager
{
public:
    void save_image(canvas& canvas);
    void load_image(canvas& canvas);
};