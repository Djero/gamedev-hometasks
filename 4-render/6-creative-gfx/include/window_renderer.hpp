#pragma once

#include "SDL.h"
#include "canvas.hpp"

class window_renderer
{
public:
    window_renderer(canvas& canvas);
    ~window_renderer();

    int render_in_window(canvas& canvas);

private:
    SDL_Window* window_;
    SDL_Renderer* renderer_;
};