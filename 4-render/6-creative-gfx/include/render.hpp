#pragma once

#include "canvas.hpp"
#include "igfx_processor.hpp"
#include "position.hpp"

class render
{
public:
    canvas* get_canvas();
    void    set_canvas(canvas* canvas);
    void    set_gfx_processor(igfx_processor* gfx_processor);
    void    set_color_to_render(color color);

    void clear_white();
    void clear_black();

    void render_line(position start, position end);
    void render_triangle(position v1, position v2, position v3);
    void render_triangles(std::vector<position> vertexes,
                          std::vector<uint8_t>  indexes);
    void render_grid(size_t cell_count);
    void render_processed_canvas();

    std::vector<vertex> rasterize_line(vertex start, vertex end);
    std::vector<vertex> rasterize_triangle(vertex v1, vertex v2, vertex v3);

private:
    canvas*         canvas_;
    igfx_processor* gfx_processor_;
    color           color_;

    double interpolate(double a, double b, double t);
    vertex interpolate(vertex v1, vertex v2, double t);
};