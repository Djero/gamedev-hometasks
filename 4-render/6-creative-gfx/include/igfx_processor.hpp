#pragma once

#include <cstddef>
#include <vector>

#include "canvas.hpp"
#include "color.hpp"
#include "uniforms.hpp"
#include "vertex.hpp"

class igfx_processor
{
public:
    virtual ~igfx_processor() = default;

    virtual vertex vertex_shader(vertex vertex)   = 0;
    virtual color  fragment_shader(vertex vertex) = 0;
    virtual void   set_uniforms(uniforms u)       = 0;
};
