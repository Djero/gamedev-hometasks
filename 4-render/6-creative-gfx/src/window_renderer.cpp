#include <iostream>

#include "../include/window_renderer.hpp"
#include "SDL.h"

window_renderer::window_renderer(canvas& canvas)
{
    window_ = SDL_CreateWindow("GFX creative",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               canvas.width,
                               canvas.height,
                               SDL_WINDOW_OPENGL);
    if (window_ == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
    }

    renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);

    if (renderer_ == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
    }
}

window_renderer::~window_renderer() {
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
}

int window_renderer::render_in_window(canvas& canvas)
{
    using namespace std;

    void*     pixels = canvas.data();
    const int depth  = sizeof(color) * 8;
    const int pitch  = canvas.width * sizeof(color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(pixels,
                                                          canvas.width,
                                                          canvas.height,
                                                          depth,
                                                          pitch,
                                                          rmask,
                                                          gmask,
                                                          bmask,
                                                          amask);
    if (bitmapSurface == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Texture* bitmapTex =
        SDL_CreateTextureFromSurface(renderer_, bitmapSurface);

    if (bitmapTex == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_FreeSurface(bitmapSurface);

    SDL_RenderClear(renderer_);
    SDL_RenderCopy(renderer_, bitmapTex, nullptr, nullptr);
    SDL_RenderPresent(renderer_);

    SDL_DestroyTexture(bitmapTex);
}
