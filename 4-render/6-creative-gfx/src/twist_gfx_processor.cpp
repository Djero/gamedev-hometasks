#include "../include/twist_gfx_processor.hpp"
#include <cmath>
#include <iostream>

void twist_gfx_processor::set_uniforms(uniforms u)
{
    uniforms_ = u;
}

vertex twist_gfx_processor::vertex_shader(vertex v_in)
{
    vertex out = v_in;

    return out;
}

color twist_gfx_processor::fragment_shader(vertex v_in)
{
    if (uniforms_.texture->empty())
    {
        std::cerr << "error: texture is not set for twist_gfx_processor"
                  << std::endl;
    }

    color out;

    double normalized_texture_x = v_in.f5;
    double normalized_texture_y = v_in.f6;

    int physical_texture_x = static_cast<int32_t>(
        (uniforms_.texture->width - 1) * normalized_texture_x);
    int physical_texture_y = static_cast<int32_t>(
        (uniforms_.texture->height - 1) * normalized_texture_y);

    double v_x     = physical_texture_x;
    double v_y     = physical_texture_y;
    double mouse_x = uniforms_.f2;
    double mouse_y = uniforms_.f3;
    double radius  = uniforms_.f4;

    if (std::pow(v_x - mouse_x, 2) + std::pow(v_y - mouse_y, 2) <
        std::pow(radius, 2))
    {
        double l =
            std::sqrt(std::pow(v_x - mouse_x, 2) + std::pow(v_y - mouse_y, 2));

        if (l > 0)
        {
            double ln =
                std::sqrt(std::pow(radius, 2) - std::pow((l - radius), 2));

            int new_x  = ln * (v_x - mouse_x) / l + mouse_x;
            int new_y  = ln * (v_y - mouse_y) / l + mouse_y;

            out = uniforms_.texture->get_pixel_color(
                { new_x, new_y });
        }
    }
    else
    {
        out = uniforms_.texture->get_pixel_color(
            { physical_texture_x, physical_texture_y });
    }

    return out;
}