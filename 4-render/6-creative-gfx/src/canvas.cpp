#include "../include/canvas.hpp"

canvas::canvas(int width, int height)
    : width(width)
    , height(height)
{
    resize(width * height);
}

color canvas::get_pixel_color(position position)
{
    int    linear_index = width * position.y + position.x;
    color& pixel        = at(linear_index);

    return pixel;
}

void canvas::set_pixel(position position, color color_to_set)
{
    int    linear_index = width * position.y + position.x;
    color& pixel_to_set = at(linear_index);
    pixel_to_set        = color_to_set;
}

std::vector<position> canvas::get_line_pixel_positions(position start,
                                                       position end)
{
    if (start.x - end.x > 0 || start.y - end.y > 0)
    {
        return get_line_pixel_positions(end, start);
    }

    std::vector<position> line_pixels;

    int x1 = start.x;
    int y1 = start.y;
    int x2 = end.x;
    int y2 = end.y;

    int dy = y2 - y1 + 1;
    int dx = x2 - x1 + 1;

    int k          = dy;
    int signed_one = k > 0 ? 1 : -1;
    k *= signed_one; // remove sign from k
    int err = 0;

    for (int x = x1, y = y1; x != x2 + 1; x++)
    {
        line_pixels.push_back({ x, y });

        err += k;

        while (err > dx)
        {
            y += signed_one;
            err -= dx;
            line_pixels.push_back({ x, y });
        }
    }

    return line_pixels;
}
