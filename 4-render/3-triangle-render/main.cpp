#include "include/canvas.hpp"
#include "include/image_manager.hpp"
#include "include/render.hpp"

int main(int, char**)
{
    int           width  = 800;
    int           height = 600;
    canvas        canvas(width, height);
    color         color = { 255, 0, 0 };
    render        render(canvas, color);
    image_manager image_manager;

    render.clear_white();

    int step_count_x = 10;
    int step_count_y = 10;
    int step_x = (width - 1) / step_count_x;
    int step_y = (height - 1) / step_count_y;

    std::vector<position> triangles;

    for (int i = 0; i < step_count_x; ++i)
    {
        for (int j = 0; j < step_count_y; ++j)
        {
            position v0{ i * step_x, j * step_y };
            position v1{ v0.x + step_x, v0.y + step_y };
            position v2{ v0.x, v0.y + step_y };
            position v3{ v0.x + step_x, v0.y };

            triangles.push_back(v0);
            triangles.push_back(v1);
            triangles.push_back(v2);

            triangles.push_back(v0);
            triangles.push_back(v3);
            triangles.push_back(v1);
        }
    }

    render.render_triangles(triangles);

    image_manager.save_image(canvas);
}