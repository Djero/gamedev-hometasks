#include "engine/include/engine.hpp"
#include "engine/include/engine_gl_debug.hpp"
#include "vendor/glad/glad.h"
#include "vendor/stb/stb_image.hpp"

#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

bool engine::init(int              window_width,
                  int              window_height,
                  std::string_view path_to_texture,
                  std::string_view path_to_vertex_shader_src,
                  std::string_view path_to_fragment_shader_src)
{
    using namespace std;
    const int sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);

    if (sdl_init_result != 0)
    {
        cerr << "error: SDL_Init failed: " << SDL_GetError() << endl;
        return false;
    }

    // set flag to enable debug gl context BEFORE sdl window init
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window_ = SDL_CreateWindow("6-moving-texture",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               window_width,
                               window_height,
                               SDL_WINDOW_OPENGL);

    if (window_ == nullptr)
    {
        cerr << "error: SDL_CreateWindow failed: " << SDL_GetError() << endl;
        return false;
    }

    int gl_major_version   = 3;
    int gl_minor_version   = 2;
    int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_version);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_version);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window_);

    if (gl_context == nullptr)
    {
        cerr << "error: SDL_GL_CreateContext failed: " << SDL_GetError()
             << endl;
        return false;
    }

    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_version);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_version);

    clog << "OpenGL context created with version: " << gl_major_version << '.'
         << gl_minor_version << endl;

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        clog << "error: failed to initialize glad" << std::endl;
        return false;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(callback_opengl_debug, nullptr);
    glDebugMessageControl(
        GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    init_gfx_program(path_to_texture,
                     path_to_vertex_shader_src,
                     path_to_fragment_shader_src);

    return true;
}

void engine::shut_down()
{
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

bool engine::process_input(action* action)
{
    // map SDL keycodes to engine's actions
    const static std::map<SDL_Keycode, action_type> sdl_to_engine_mappings = {
        { SDLK_w, action_type::UP },       { SDLK_a, action_type::LEFT },
        { SDLK_s, action_type::BACK },     { SDLK_d, action_type::RIGHT },
        { SDLK_i, action_type::BTN1 },     { SDLK_j, action_type::BTN2 },
        { SDLK_k, action_type::BTN3 },     { SDLK_l, action_type::BTN4 },
        { SDLK_ESCAPE, action_type::QUIT }
    };

    action->act_type = action_type::UNKNOWN;
    action->pressed = false;

    // get SDL event
    SDL_Event event;
    SDL_PollEvent(&event);

    // quit on dedicated SDL event
    if (event.type == SDL_QUIT)
    {
        return false;
    }

    // we are not interested in other types of events for now
    if (event.type != SDL_KEYDOWN && event.type != SDL_KEYUP)
    {
        return true;
    }

    // look for engine's action name by SDL keycode
    auto mapping = sdl_to_engine_mappings.find(event.key.keysym.sym);

    // return if not found, but program continues running
    if (mapping == std::end(sdl_to_engine_mappings))
    {
        action->act_type = action_type::UNKNOWN;
        return true;
    }

    // quit if dedicated key is pressed
    if (action->act_type == action_type::QUIT)
    {
        return false;
    }

    // store engine's action name for future output
    action->act_type = mapping->second;

    // determine key state
    switch (event.type)
    {
        case SDL_KEYDOWN:
            action->pressed = true;
            break;
        case SDL_KEYUP:
            action->pressed = false;
            break;
    }

// BEGIN DEBUG OUTPUT

    const static std::map<action_type, std::string> act_type_to_str_map = {
        { action_type::UP, "UP" },
        { action_type::BACK, "BACK" },
        { action_type::RIGHT, "RIGHT" },
        { action_type::LEFT, "LEFT" }
    };


    std::cout << act_type_to_str_map.at(action->act_type) << " is "
              << (action->pressed ? "pressed" : "released") << std::endl;

// END DEBUG OUTPUT

    return true;
};

void engine::swap_buffers()
{
    SDL_GL_SwapWindow(window_);
}

void engine::clear_window(uint8_t r, uint8_t g, uint8_t b)
{
    auto r_gl_float = static_cast<GLfloat>(r);
    auto g_gl_float = static_cast<GLfloat>(g);
    auto b_gl_float = static_cast<GLfloat>(b);

    GLfloat r_normalized = 1.f / 255.f * r_gl_float;
    GLfloat g_normalized = 1.f / 255.f * g_gl_float;
    GLfloat b_normalized = 1.f / 255.f * b_gl_float;

    glClearColor(r_normalized, g_normalized, b_normalized, 0.0f);
    GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT);
    GL_CHECK()
};

void engine::render_triangle(vertex& v1, vertex& v2, vertex& v3)
{
    std::array<vertex, 3> triangle = { v1,
                                       v2,
                                       v3 }; // TODO: check the array content

    // fill data for the first vertex shader parameter (attribute)
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), &triangle[0].x);
    GL_CHECK()

    glVertexAttribPointer(
        1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), &triangle[0].u);

    glEnableVertexAttribArray(0);
    GL_CHECK()

    glEnableVertexAttribArray(1);
    GL_CHECK()

    glDrawArrays(GL_TRIANGLES, 0, 3);
    GL_CHECK()
}

void engine::init_gfx_program(std::string_view path_to_texture,
                              std::string_view path_to_vertex_shader_src,
                              std::string_view path_to_fragment_shader_src)
{
    init_texture(path_to_texture);

    GLuint vertex_shader_id =
        init_shader(GL_VERTEX_SHADER, path_to_vertex_shader_src);

    if (vertex_shader_id == 0)
    {
        std::cerr << "error: can't init gl program: vertex shader init failed"
                  << std::endl;
    }

    GLuint fragment_shader_id =
        init_shader(GL_FRAGMENT_SHADER, path_to_fragment_shader_src);

    if (fragment_shader_id == 0)
    {
        std::cerr << "error: can't init gl program: fragment shader init failed"
                  << std::endl;
    }

    gl_program_id_ = glCreateProgram();
    GL_CHECK()

    if (gl_program_id_ == 0)
    {
        std::cerr << "error: can't init gl program: glCreateProgram failed"
                  << std::endl;
    }

    glAttachShader(gl_program_id_, vertex_shader_id);
    GL_CHECK()

    glAttachShader(gl_program_id_, fragment_shader_id);
    GL_CHECK()

    glLinkProgram(gl_program_id_);
    GL_CHECK()

    glUseProgram(gl_program_id_);
    GL_CHECK()
}

bool engine::init_texture(std::string_view path)
{
    // load .png file from filesystem into container of bytes
    std::ifstream file(path.data(), std::ios::binary);
    int           width, height, bits_per_pixel;

    stbi_set_flip_vertically_on_load(1);
    unsigned char* texture =
        stbi_load(path.data(), &width, &height, &bits_per_pixel, 3);

    // Generate texture name
    GLuint texture_id = 0;
    glGenTextures(1, &texture_id);
    GL_CHECK()

    // Select the texture in OpenGL context by name
    glBindTexture(GL_TEXTURE_2D, texture_id);
    GL_CHECK()

    // Set params for texture
    glTexParameteri(GL_TEXTURE_2D,
                    GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST); // TODO: Try GL_LINEAR
    GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D,
                    GL_TEXTURE_MAG_FILTER,
                    GL_NEAREST); // TODO: Try GL_LINEAR
    GL_CHECK()

    // Send data to OpenGL
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 width,
                 height,
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 texture);
    GL_CHECK()

    return true;
}

GLuint engine::init_shader(GLenum shader_type, std::string_view shader_src_path)
{
    if (shader_type != GL_VERTEX_SHADER && shader_type != GL_FRAGMENT_SHADER)
    {
        std::cerr << "error: can't init shader with shader type: "
                  << shader_type << std::endl;
        return 0;
    }

    GLuint shader_id = glCreateShader(shader_type);
    GL_CHECK()

    std::string src         = load_shader_src(shader_src_path);
    const char* src_pointer = src.c_str();

    glShaderSource(shader_id,
                   1,
                   &src_pointer,
                   nullptr); // TODO: try pass shader_src directly
    GL_CHECK()

    glCompileShader(shader_id);
    GL_CHECK()

    // TODO: Add check for compiling status

    return shader_id;
}

std::string engine::load_shader_src(std::string_view path)
{
    std::ifstream      file(path.data());
    std::ostringstream shader_src_buf;

    shader_src_buf << file.rdbuf();

    return shader_src_buf.str();
}

iengine* create_engine()
{
    iengine* e = new engine();
    return e;
};

void destroy_engine(iengine* engine)
{
    delete engine;
};