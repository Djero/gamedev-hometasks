#include "engine/include/hot_loader.hpp"

#include <filesystem>
#include <iostream>
#include <vector>

#include <SDL.h>

hot_loader::hot_loader(std::string& libName)
    : lib_name((new std::string("./lib" + libName + ".so"))->c_str())
    , tmp_lib_name((new std::string("./tmplib" + libName + ".so"))->c_str())
{
}

bool hot_loader::is_game_updated()
{
    try
    {
        auto lib_current_update_time =
            std::filesystem::last_write_time(lib_name);
//
//      TODO: Find out why doesn't work on Fedora
//
//        std::time_t lib_current_update_time_ =
//            decltype(lib_current_update_time)::clock::to_time_t(
//                lib_current_update_time);

        if (lib_current_update_time != lib_last_update_time) // TODO
        {
            lib_last_update_time = lib_current_update_time;
            return true;
        }
    }
    catch (std::exception& ex)
    {
        std::cout << ex.what();
    }

    return false;
}

igame* hot_loader::load_game(iengine* engine)
{
    // unload tmp lib
    if (old) // TODO
    {
        unload_game();
        SDL_UnloadObject(tmp_lib_handle);
    }

    // remove tmp lib
    if (std::filesystem::exists(tmp_lib_name))
    {
        if (!std::filesystem::remove(tmp_lib_name))
        {
            std::cerr << "Error: can't remove " << tmp_lib_name << std::endl;
            return nullptr;
        }
    }

    // copy tmp lib
    try
    {
        std::filesystem::copy(lib_name, tmp_lib_name);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Error: can't copy [" << lib_name << "] to ["
                  << tmp_lib_name << ']' << std::endl;
        return nullptr;
    }

    // load tmp lib
    void* new_tmp_lib_handle = SDL_LoadObject(tmp_lib_name);

    if (new_tmp_lib_handle == nullptr)
    {
        std::cerr << "Error: failed to load [" << tmp_lib_name << ']'
                  << SDL_GetError() << std::endl;
        return nullptr;
    }

    tmp_lib_handle = new_tmp_lib_handle;

    void* create_game_lib_fn_ptr = SDL_LoadFunction(tmp_lib_handle, "create_game");

    if (create_game_lib_fn_ptr == nullptr)
    {
        std::cerr << "Error: no function [create_game] in [" << tmp_lib_name
                  << ']' << std::endl;
        return nullptr;
    }

    typedef decltype(&create_game) create_game_fn;

    auto create_game_fn_ptr_local =
        reinterpret_cast<create_game_fn>(create_game_lib_fn_ptr);

    igame* new_game = create_game_fn_ptr_local(engine);

    if (new_game == nullptr)
    {
        std::cerr << "Error: function [create_game] returned nullptr"
                  << std::endl;
        return nullptr;
    }

    old = new_game;

    return new_game;
}

void hot_loader::unload_game()
{
    void* destroy_game_lib_fn_ptr =
        SDL_LoadFunction(tmp_lib_handle, "destroy_game");

    if (destroy_game_lib_fn_ptr == nullptr)
    {
        std::cout << "Error: no function [destroy_game] in [" << tmp_lib_name
                  << ']' << std::endl;
        return;
    }

    typedef decltype(&destroy_game) destroy_game_lib_fn;

    auto destroy_game_lib_fn_ptr_local =
        reinterpret_cast<destroy_game_lib_fn>(destroy_game_lib_fn_ptr);

    destroy_game_lib_fn_ptr_local(old);
}