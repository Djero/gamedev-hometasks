#pragma once

#include "iengine.hpp"

#include <chrono>
#include <string>

class igame;

class hot_loader
{
public:
    hot_loader(std::string& libName);
    bool   is_game_updated();
    igame* load_game(iengine* engine);
    void   unload_game();

private:
    igame*      old            = nullptr;
    const char* lib_name       = nullptr;
    const char* tmp_lib_name   = nullptr;
    void*       tmp_lib_handle = nullptr;
    std::chrono::time_point<std::filesystem::__file_clock> lib_last_update_time;
};
