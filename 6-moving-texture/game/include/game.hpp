#include "engine/include/iengine.hpp"

#include <iostream>
#include <vector>

class game : public igame
{
public:
    game(iengine* engine);

    bool init() override;
    void shut_down() override;

    void on_action(action& action) override;
    void simulate() override;
    void render() override;

private:
    iengine*            engine_ = nullptr;
    std::vector<vertex> geometry_;
    // clang-format off
    float move_matrix_[3][3] =
        {
            {1.f, 0.f, 0.f},
            {0.f, 1.f, 0.f},
            {0.f, 0.f, 1.f}
        };
    // clang-format on
};
