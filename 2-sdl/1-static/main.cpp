#include <iostream>
#include <SDL_version.h>

int main(int, char**)
{
    SDL_version compiled;
    SDL_version linked;

    SDL_VERSION(&compiled);
    SDL_GetVersion(&linked);

    std::cout << "Compiled against SDL version: "
              << unsigned(compiled.major)
              << "."
              << unsigned(compiled.minor)
              << "."
              << unsigned(compiled.patch)
              << std::endl;

    std::cout << "Linked against SDL version: "
              << unsigned(linked.major)
              << "."
              << unsigned(linked.minor)
              << "."
              << unsigned(linked.patch)
              << std::endl;

    return std::cout.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}
