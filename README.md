**_master_ last build status:** [![pipeline status](https://gitlab.com/Djero/gamedev-hometasks/badges/master/pipeline.svg)](https://gitlab.com/Djero/gamedev-hometasks/-/commits/master)

# Home tasks for game development classes

**Note:** All the home tasks are stored in dedicated folders.

You can find notes about each of the implemented home tasks below.

## 1-hellow-world/1-exe
Classic simple "hello world" program built via cmake as single executable.

## 1-hello-world/2-libs 
The same "hello world" program, but built as library and invoked in main.cpp. There are 2 version implemented - statically linked library and dynamically linked library.

### CI/CD 
There 2 approaches implemented to integrate the 1-hello-world/2-libs task into GitLab CI pipline. Both of them take advantage of Docker containers and GitLab CI/CD. You can find *Dockerfile* and *.gitlab-ci.yml* files for each of approaches in dedicated folders - *1-hello-world/2-libs/ci-cd/approach-1* and *1-hello-world/2-libs/ci-cd/approach-2*. 

***Approach number 2 is currently active***.

**1-st approach:** All the environment setup, build, test and run operations defined in single Dockerfile. GitLab pipline is used only for invoking Docker. The *Dockerfile* of this approach better suits for local run of imlemented program in Docker.

**2-nd approach:** All the classic CI tasks are defined in *.gitlab-ci* file. *Dockerfile* just defined the environment for building, testing and running program.

## 2-sdl/1-static
Simple version output of SDL2 linked statically.
    
## 2-sdl/2-dynamic
Simple version output of SDL2 linked dynamically.