#include "engine/sound_buffer.hpp"

#include <SDL.h>
#include <algorithm>
#include <iostream>
#include <string>

isound_buffer::~isound_buffer() = default;

sound_buffer::~sound_buffer()
{
    if (!tmp_buf)
    {
        SDL_FreeWAV(buffer);
    }
    buffer = nullptr;
    length = 0;
}

sound_buffer::sound_buffer(std::string_view  path,
                           SDL_AudioDeviceID device_,
                           SDL_AudioSpec     device_audio_spec)
    : buffer(nullptr)
    , length(0)
    , device(device_)
{
    SDL_AudioSpec file_audio_spec;
    SDL_RWops*    file = SDL_RWFromFile(path.data(), "rb");

    if (file == nullptr)
    {
        std::cerr << "error: can't open file" << path.data() << std::endl
                  << SDL_GetError() << std::endl;
        return;
    }

    // TODO: replace func call with macro
    if (SDL_LoadWAV_RW(file, 1, &file_audio_spec, &buffer, &length) == nullptr)
    {
        std::cerr << "error: can't load WAV" << path.data() << std::endl
                  << SDL_GetError() << std::endl;
        return;
    }

    if (file_audio_spec.channels != device_audio_spec.channels ||
        file_audio_spec.format != device_audio_spec.format ||
        file_audio_spec.freq != device_audio_spec.freq)
    {
        SDL_AudioCVT cvt;
        SDL_BuildAudioCVT(&cvt,
                          file_audio_spec.format,
                          file_audio_spec.channels,
                          file_audio_spec.freq,
                          device_audio_spec.format,
                          device_audio_spec.channels,
                          device_audio_spec.freq);

        // TODO: refactor
        if (cvt.needed) // obviously, this one is always needed.
        {
            // read your data into cvt.buf here.
            cvt.len = static_cast<int>(length);
            // we have to make buffer for inplace conversion
            size_t new_buffer_size =
                static_cast<size_t>(cvt.len * cvt.len_mult);
            tmp_buf.reset(new uint8_t[new_buffer_size]);
            uint8_t* buf_tmp = tmp_buf.get();
            // copy old buffer to new memory
            std::copy_n(buffer, length, buf_tmp);
            // cvt.buf has cvt.len_cvt bytes of converted data now.
            SDL_FreeWAV(buffer);
            cvt.buf = buf_tmp;
            if (0 != SDL_ConvertAudio(&cvt))
            {
                std::cout << "failed to convert audio from file: " << path
                          << " to audio device format" << std::endl;
            }

            buffer = tmp_buf.get();
            length = static_cast<uint32_t>(cvt.len_cvt);
        }
    }
}

void sound_buffer::play(bool looped)
{
    // Lock callback function
    SDL_LockAudioDevice(device); //TODO: Replace with mutex

    // here we can change properties
    // of sound and dont collade with multithreaded playing
    current_index = 0;
    is_playing    = true;
    is_looped     = looped;

    // unlock callback for continue mixing of audio
    SDL_UnlockAudioDevice(device); //TODO: Replace with mutex
}
