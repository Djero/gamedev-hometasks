#include "include/hot_loader.hpp"
#include "include/iengine.hpp"

#include <cstdlib>
#include <iostream>
#include <string>

int main(int, char**)
{
    std::string game_lib_name = "game";
    hot_loader  hot_loader(game_lib_name);
    iengine* engine = create_engine();

    if(!engine->init())
    {
        std::cerr << "error: failed to init an engine" << std::endl;
        return EXIT_FAILURE;
    }

    igame* game = hot_loader.load_game(engine);

    action action{};

    while (engine->process_input(action))
    {
        if (hot_loader.is_game_updated())
        {
            game = hot_loader.load_game(engine);
        }

        game->on_action();
        game->simulate();
        game->render();
    }

    hot_loader.unload_game();
    engine->shut_down();
    destroy_engine(engine);

    return EXIT_SUCCESS;
}