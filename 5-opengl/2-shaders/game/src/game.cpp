#include <iostream>

#include "../../engine/include/iengine.hpp"
#include "../include/game.hpp"

game::game(iengine* engine)
    : engine_(engine)
{
}

void game::on_action() {}

void game::simulate() {}

void game::render()
{
    vertex v1 = {-0.5, 0.5, -0.5};
    vertex v2 = {0.5, 0.5, 0.5};
    vertex v3 = {0.5, -0.5, -0.5};
    vertex v4 = {-0.5, -0.5, -0.5};

    engine_->swap_buffers();
    engine_->clear_window(255,255,255);
    engine_->render_triangle(v1, v3, v4);
    engine_->render_triangle(v1, v2, v3);
}

igame* create_game(iengine* engine)
{
    auto g = new game(engine);
    return g;
}

void destroy_game(igame* game)
{
    delete game;
}