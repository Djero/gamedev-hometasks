#pragma once

#include "engine/render/vertex.hpp"
#include <string>

enum class drum_type
{
    SNARE,
    BASS,
    HAT,
    CRASH,
    RIDE
};

class drum_note
{
public:
    vertex v1;
    vertex v2;
    vertex v3;
    vertex v4;

    float hit_time;
    bool  is_played = false;

    drum_type   type;
    std::string path_to_texture;
};
