#pragma once

#include "index_buffer.hpp"
#include "shader.hpp"
#include "vertex_array.hpp"

#include "SDL.h"

class renderer
{
public:
    void draw(const vertex_array& va,
              const index_buffer& ib,
              const shader&       shader) const;
    void clear() const;
    void clear(uint8_t r, uint8_t g, uint8_t b) const;

private:
};