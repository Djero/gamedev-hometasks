#pragma once

#include <string>

class shader
{
public:
    shader(const std::string& path_to_vertex_shader_src,
           const std::string& path_to_fragment_shader_src);
    ~shader();

    void bind() const;
    void unbind() const;

    void set_uniform_1i(const std::string& name, int value);
    void set_uniform_4f(const std::string& name,
                        float              value1,
                        float              value2,
                        float              value3,
                        float              value4);

private:
    std::string  parse_shader(const std::string& path_to_src);
    unsigned int compile_shader(unsigned int type, const std::string& source);
    unsigned int create_shader(const std::string& vertex_shader_src,
                               const std::string& fragment_shader_src);
    unsigned int get_uniform_location(const std::string& name);

    unsigned int gl_shader_id_;
};
