#pragma once

#include "SDL.h"

class fps
{
private:
    int value_;
    int delay_time;
    Uint32 next_frame_start;

public:
    fps(int fps);
    void start();
    void wait();
};