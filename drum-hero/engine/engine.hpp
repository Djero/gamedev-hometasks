#pragma once

#include "audio/audio_buffer.hpp"
#include "engine/input/input_processor.hpp"
#include "iengine.hpp"

#include "fps.hpp"
#include "vendor/glad/glad.h"

#include <SDL.h>
#include <engine/render/renderer.hpp>
#include <string>
#include <string_view>

class engine : public iengine
{
private:
    renderer        renderer_;
    input_processor input_processor_;
    SDL_Window*     window_        = nullptr;

    std::string path_to_vertex_shader_src_;
    std::string path_to_fragment_shader_src_;

    SDL_AudioDeviceID          audio_device;
    SDL_AudioSpec              audio_device_spec;
    std::vector<audio_buffer*> sounds;
    static void                audio_callback(void*, uint8_t*, int);

public:
    // init / shut down
    bool init(const std::string& game_name,
              int                window_width,
              int                window_height,
              const std::string& path_to_vertex_shader_src,
              const std::string& path_to_fragment_shader_src) override;
    void shut_down() override;

    // input
    bool process_input(std::vector<action>* actions) override;

    // render
    void swap_buffers() override;
    void clear_window(uint8_t r, uint8_t g, uint8_t b) override;
    void render(std::vector<vertex>&       vertexes,
                std::vector<unsigned int>& indexes,
                std::string&               path_to_texture) override;

    inline unsigned int get_milisec_count_from_init() override { return SDL_GetTicks(); } //TODO: remove from header

    // audio
    iaudio_buffer* create_sound_buffer(std::string_view path) override;
    void           destroy_sound_buffer(iaudio_buffer*) override;

private:
    void init_audio();
};