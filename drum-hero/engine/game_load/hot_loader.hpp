#pragma once

#include "engine/iengine.hpp"

#include <chrono>
#include <filesystem>
#include <string>

class igame;

class hot_loader
{
public:
    hot_loader(std::string& libName);
    bool   is_game_updated();
    igame* load_game(iengine* engine);
    void   unload_game();

private:
    igame*      old            = nullptr;
    const char* lib_name       = nullptr;
    const char* tmp_lib_name   = nullptr;
    void*       tmp_lib_handle = nullptr;
    std::filesystem::file_time_type lib_last_update_time;
};
