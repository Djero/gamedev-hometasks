#pragma once

#include "iaudio_buffer.hpp"

#include <SDL.h>

#include <memory>
#include <string_view>

#pragma pack(push, 1) // TODO: Investigate removing
class audio_buffer : public iaudio_buffer
{
public:
    audio_buffer(std::string_view  path,
                 SDL_AudioDeviceID device_,
                 SDL_AudioSpec     device_audio_spec);
    ~audio_buffer();

    void play(bool looped) override;

    std::unique_ptr<uint8_t[]> tmp_buf;
    uint8_t*                   buffer;
    uint32_t                   length;
    uint32_t                   current_index = 0;
    SDL_AudioDeviceID          device;
    bool                       is_playing = false;
    bool                       is_looped  = false;
};
#pragma pack(pop)