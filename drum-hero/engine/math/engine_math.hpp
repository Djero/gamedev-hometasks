#pragma once

float interpolate(float a, float b, float t)
{
    return a + (b - a) * t;
}