#include "engine/Engine.hpp"

int main(int, char**)
{
    Engine* engine = ResolveEngine();
    engine->Init();
    Action action;

    while (engine->ProcessInput(action))
    {
        engine->SetOutput(action);
    }

    engine->ShutDown();
    DestructEngine(engine);
}