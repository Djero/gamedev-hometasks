#pragma once

enum class ActionName
{
    UNKNOWN,
    QUIT,
    UP,
    LEFT,
    BACK,
    RIGHT,
    BTN1,
    BTN2,
    BTN3,
    BTN4
};

class Action
{
public:
    ActionName action_name;
    bool       pressed;
};

class Engine
{
public:
    virtual ~Engine();
    virtual bool Init()                       = 0;
    virtual void ShutDown()                   = 0;
    virtual bool ProcessInput(Action& action) = 0;
};

class Game
{
public:
    virtual ~Game()                       = default;
    virtual void OnAction(Action& action) = 0;
    virtual void Simulate()               = 0;
    virtual void Render()                 = 0;
};

Engine* ResolveEngine();
void    DestructEngine(Engine* engine);

extern "C"
{
    Game* CreateGame();
    void  DestructGame();
}