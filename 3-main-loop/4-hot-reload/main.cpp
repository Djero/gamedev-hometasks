#include <cstdlib>

#include "Engine/Engine.hpp"
#include "Engine/HotLoader.hpp"

int main(int, char**)
{
    std::string gameLibName = "game-lib";
    HotLoader   hotLoader(gameLibName);
    Action      action;

    Engine* engine = ResolveEngine();
    engine->Init();
    Game* game = hotLoader.LoadGame();

    while (engine->ProcessInput(action))
    {
        if (hotLoader.IsGameUpdated())
        {
            game = hotLoader.LoadGame();
        }

        game->OnAction(action);
        game->Simulate();
        game->Render();
    }

    hotLoader.UnloadGame();
    engine->ShutDown();
    DestructEngine(engine);

    return EXIT_SUCCESS;
}